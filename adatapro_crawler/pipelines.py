import logging
import sqlite3


class AdataproCrawlerPipeline:
    def __init__(self):
        """
        Creating a local sqlite database and
        creating the schema if it doesn't exist already. 
        """
        self.connection = sqlite3.connect("./ardesdata.db")
        self.cursor = self.connection.cursor()
        self.cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS ArdesData
            (
                id INTEGER PRIMARY KEY, 
                title VARCHAR(255), 
                rating REAL, 
                parameters TEXT(1000), 
                price REAL
            )
        """
        )

    def process_item(self, item, spider):
        """
        Minimal item processing and inserting it into the
        database using the `self.connection` initialized
        in the constructor.
        """
        
        # SQLite doesn't support arrays natively so instead
        # we save the parameters to comma seperated strings(csv)
        # Example output: `Intel Core i7-1165G7, NVIDIA MX330 2 GB GDDR5, ...`
        parameters_to_text = ", ".join(item["parameters"])
        self.cursor.execute(
            """INSERT INTO ArdesData (title, rating, parameters, price) values (?, ?, ?, ?)""",
            (item["title"], item["rating"], parameters_to_text, item["price"]),
        )
        self.connection.commit()
        logging.debug("Item stored {}".format(item["title"]))
        return item
